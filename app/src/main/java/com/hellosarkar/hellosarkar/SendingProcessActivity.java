package com.hellosarkar.hellosarkar;

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.TextView;

import com.hellosarkar.hellosarkar.base.BaseActivity;
import com.hellosarkar.hellosarkar.utilities.Opener;

import butterknife.BindView;

public class SendingProcessActivity extends BaseActivity {

    public ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    DrawerLayout drawer;

    @BindView(R.id.title_text_process)
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        createToolbar();
        title.setText(getApplicationContext().getString(R.string.tracker));
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Ananda_Lipi_Bold_Cn_Bt.ttf");
        title.setTypeface(tf);

    }

    private void createToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.process_drayer_id);
//        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.setHomeAsUpIndicator(R.color.white);
        drawer.setDrawerListener(toggle);


        toggle.syncState();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_sending_process;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else  {
            Opener.openMainActivity(SendingProcessActivity.this);
        }
    }
}
