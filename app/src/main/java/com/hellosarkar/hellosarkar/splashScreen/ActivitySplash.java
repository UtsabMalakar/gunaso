package com.hellosarkar.hellosarkar.splashScreen;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.hellosarkar.hellosarkar.R;
import com.hellosarkar.hellosarkar.base.BaseActivity;
import com.hellosarkar.hellosarkar.utilities.Opener;

import butterknife.BindView;
import butterknife.Optional;

public class ActivitySplash extends BaseActivity {
    @BindView(R.id.image_splash)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        //imageView.setAnimation(zoomin);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Opener.openMainActivity(ActivitySplash.this);
            }
        }, 2000);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }
}
