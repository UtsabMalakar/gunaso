package com.hellosarkar.hellosarkar;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellosarkar.hellosarkar.base.BaseActivity;
import com.hellosarkar.hellosarkar.utilities.Opener;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    public ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    DrawerLayout drawer;

    @BindView(R.id.title_text)
    TextView tv_title;

    @BindView(R.id.menu1)
    ImageView im_feedback;

    @BindView(R.id.menu2)
    ImageView tracker;

    @BindView(R.id.menu3)
    ImageView medium;

    @BindView(R.id.menu4)
    ImageView process;

    @BindView(R.id.recent_news)
    TextView recentNews;

    @OnClick(R.id.menu1)
    void openFeedback() {
        Opener.openNewFeedbackActivity(MainActivity.this);
//        FeedbackFragment addressFragment = new FeedbackFragment();
//        getSupportFragmentManager().beginTransaction().
//                setCustomAnimations(R.anim.fade_out, R.anim.fade_out)
//                .replace(R.id.sc_main, addressFragment).commit();
    }

    @OnClick(R.id.menu2)
    void openTracker() {
        Opener.openTrackerActivity(MainActivity.this);

    }

    @OnClick(R.id.menu3)
    void openMedium() {
        Opener.openMediumActivity(MainActivity.this);

    }

    @OnClick(R.id.menu4)
    void openProcess() {
        Opener.openProcessActivity(MainActivity.this);

    }

    @OnClick(R.id.recent_news)
    void openNewsList() {
        Opener.openNewsListActivity(MainActivity.this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        createToolbar();

        tv_title.setText(R.string.helloSarkar);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Ananda_Lipi_Bold_Cn_Bt.ttf");
        tv_title.setTypeface(tf);
    }

    private void createToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.menu_drayer_id);
//        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.setHomeAsUpIndicator(R.color.white);
        drawer.setDrawerListener(toggle);


        toggle.syncState();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
