package com.hellosarkar.hellosarkar;

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.TextView;

import com.hellosarkar.hellosarkar.Adapters.NewsRecyclerAdapter;
import com.hellosarkar.hellosarkar.base.BaseActivity;
import com.hellosarkar.hellosarkar.utilities.Opener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by pradip on 12/20/16.
 */

public class NewsListActivity extends BaseActivity {

    public ActionBarDrawerToggle toggle;

    int[] news = {R.string.news_article, R.string.news_article, R.string.news_article};
    int[] image = {R.drawable.newspic, R.drawable.newspic, R.drawable.newspic};
    int[] newsTitle = {R.string.title1, R.string.title2, R.string.title3};
    int[] date = {R.string.date1, R.string.date2, R.string.date3};
    RecyclerView.Adapter newsRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    NewsRecyclerAdapter mAdapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.news_drawer_id)
    DrawerLayout drawer;

    @BindView(R.id.title_text_recent_news)
    TextView title;

    @BindView(R.id.news_list_recycler_view)
    RecyclerView newsListRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        createToolbar();
        title.setText(getApplicationContext().getString(R.string.recent_news));
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Ananda_Lipi_Bold_Cn_Bt.ttf");
        title.setTypeface(tf);


        newsListRecyclerView.hasFixedSize();
        layoutManager = new LinearLayoutManager(this);
        newsListRecyclerView.setLayoutManager(layoutManager);
        newsRecyclerAdapter = new NewsRecyclerAdapter(getNewsData());
        newsListRecyclerView.setAdapter(newsRecyclerAdapter);


    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        mAdapter.setOnItemClickListener(new NewsRecyclerAdapter.MyClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
//                Log.i("LOG_TAG", " Clicked on Item " + position);
//            }
//        });
//    }

    private void createToolbar() {

        setSupportActionBar(toolbar);
//        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.setHomeAsUpIndicator(R.color.white);
        drawer.setDrawerListener(toggle);


        toggle.syncState();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_news_list;

    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Opener.openMainActivity(NewsListActivity.this);
        }
    }

    public List<NewsDataTest> getNewsData() {
        List<NewsDataTest> newsDataTestList = new ArrayList<>();
        int i, j;
        for (i=0;i<5;i++) {
            for (j=0;j<news.length;j++) {
                NewsDataTest newsDataTest = new NewsDataTest(news[j], image[j], date[j], newsTitle[j]);
                newsDataTestList.add(newsDataTest);
            }
        }

        return newsDataTestList;
    }
}
