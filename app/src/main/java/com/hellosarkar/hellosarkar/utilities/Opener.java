package com.hellosarkar.hellosarkar.utilities;

import android.app.Activity;
import android.content.Intent;

import com.hellosarkar.hellosarkar.FeedbackActivity;
import com.hellosarkar.hellosarkar.FullNewsActivity;
import com.hellosarkar.hellosarkar.MainActivity;
import com.hellosarkar.hellosarkar.NewsDataTest;
import com.hellosarkar.hellosarkar.NewsListActivity;
import com.hellosarkar.hellosarkar.R;
import com.hellosarkar.hellosarkar.SendMediumActivity;
import com.hellosarkar.hellosarkar.SendingProcessActivity;
import com.hellosarkar.hellosarkar.TrackerActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebpearls on 7/4/2016.
 */
public class Opener {
    static Intent intent;

    public static void openMainActivity(Activity activity) {
        intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);

    }

    public static void openNewFeedbackActivity(Activity activity) {

        intent = new Intent(activity, FeedbackActivity.class);
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);

    }

    public static void openTrackerActivity(Activity activity) {

        intent = new Intent(activity, TrackerActivity.class);
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);

    }

    public static void openMediumActivity(Activity activity) {

        intent = new Intent(activity, SendMediumActivity.class);
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);

    }

    public static void openProcessActivity(Activity activity) {

        intent = new Intent(activity, SendingProcessActivity.class);
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);

    }

    public static void openNewsListActivity(Activity activity) {
        intent = new Intent(activity, NewsListActivity.class);
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);
    }

//    public static void openFullNewsActivity(Activity activity, int news, int image, int dateTime) {
//        intent = new Intent(activity, FullNewsActivity.class);
//        intent.putExtra("NEWS", news);
//        intent.putExtra("IMAGE", image);
//        intent.putExtra("DATE", dateTime);
//        activity.startActivity(intent);
//        activity.finish();
//        activity.overridePendingTransition(0, R.anim.fade_out);
//    }

    public static void openFullNewsActivity(Activity activity, List<NewsDataTest> list, int adapterPosition) {
        ArrayList<NewsDataTest> itemList = new ArrayList<>();

        intent = new Intent(activity, FullNewsActivity.class);
        itemList = (ArrayList<NewsDataTest>) list;

        intent.putExtra("ADAPTER_POSITION", adapterPosition);
        intent.putParcelableArrayListExtra("ITEM_LIST", itemList);

        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(0, R.anim.fade_out);
    }


}
