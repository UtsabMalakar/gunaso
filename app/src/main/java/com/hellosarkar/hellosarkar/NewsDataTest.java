package com.hellosarkar.hellosarkar;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradip on 12/21/16.
 */

public class NewsDataTest implements Parcelable{

    private int news;
    private int image;
    private int dateTime;
    private int newsTitle;

    public NewsDataTest(int news, int image, int dateTime, int newsTitle) {
        this.news = news;
        this.image = image;
        this.dateTime = dateTime;
        this.newsTitle = newsTitle;
    }

    public NewsDataTest(Parcel in) {
        news = in.readInt();
        image = in.readInt();
        dateTime = in.readInt();
        newsTitle = in.readInt();
    }

    public static final Parcelable.Creator<NewsDataTest> CREATOR = new Parcelable.Creator<NewsDataTest>() {
        @Override
        public NewsDataTest createFromParcel(Parcel in) {
            return new NewsDataTest(in);
        }

        @Override
        public NewsDataTest[] newArray(int size) {
            return new NewsDataTest[size];
        }
    };

    public int getNews() {
        return news;
    }

    public void setNews(int news) {
        this.news = news;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getDateTime() {
        return dateTime;
    }

    public void setDateTime(int dateTime) {
        this.dateTime = dateTime;
    }

    public int getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(int newsTitle) {
        this.newsTitle = newsTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(news);
        parcel.writeInt(image);
        parcel.writeInt(dateTime);
        parcel.writeInt(newsTitle);
    }
}
