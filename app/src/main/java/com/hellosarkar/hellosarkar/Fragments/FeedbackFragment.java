//package com.hellosarkar.hellosarkar.Fragments;
//
//
//import android.content.Context;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.hellosarkar.hellosarkar.Adapters.FeedbackArrayAdapter;
//import com.hellosarkar.hellosarkar.MainActivity;
//import com.hellosarkar.hellosarkar.R;
//import com.hellosarkar.hellosarkar.RetrofitModel.AreaModel;
//import com.hellosarkar.hellosarkar.RetrofitModel.DataClass;
//import com.hellosarkar.hellosarkar.RetrofitModel.DataZone;
//import com.hellosarkar.hellosarkar.RetrofitModel.DistrictData;
//import com.hellosarkar.hellosarkar.RetrofitModel.Ministry;
//import com.hellosarkar.hellosarkar.RetrofitModel.PostModel;
//import com.hellosarkar.hellosarkar.RetrofitModel.ZoneData;
//import com.hellosarkar.hellosarkar.base.BaseFragment;
//import com.hellosarkar.hellosarkar.utilities.KeyboardHelper;
//import com.hellosarkar.hellosarkar.utilities.Library;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import butterknife.BindView;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//
//public class FeedbackFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {
//    public String zoneId, regionId, deptId, districtId, sexId;
//    //Binding Views
//    @BindView(R.id.tv_titlef)
//    TextView tv_title;
//    @BindView(R.id.sp_departmentf)
//    Spinner sp_department;
//    @BindView(R.id.sp_zonef)
//    Spinner sp_zone;
//    @BindView(R.id.sp_areaf)
//    Spinner sp_Area;
//    @BindView(R.id.sp_districtf)
//    Spinner sp_district;
//    @BindView(R.id.sp_sexf)
//    Spinner sp_sex;
//    @BindView(R.id.et_subjectf)
//    EditText et_subject;
//    @BindView(R.id.et_app_namef)
//    EditText et_app_name;
//    @BindView(R.id.et_addressf)
//    EditText et_address;
//    @BindView(R.id.et_discriptionf)
//    EditText et_description;
//    @BindView(R.id.btn_proceedf)
//    Button btn_proceed;
//    MainActivity activity;
//    //Lists
//    private List<String> ddept_list;
//    private List<String> dev_region_list;
//    private List<String> zones_list;
//    private List<String> district_list;
//    private List<String> sex;
//    private Map<Integer, String> areaId;
//    private Map<Integer, String> id_zone;
//    private Map<Integer, String> id_district;
//    private Map<Integer, String> id_dept;
//    private Library library;
//    private KeyboardHelper kb_helper;
//    private boolean check = false;
//    private Context context;
//
//    public FeedbackFragment() {
//        // Required empty public constructor
//    }
//
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        this.activity = (MainActivity) context;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        View rootview = inflater.inflate(R.layout.fragment_feedback, container, false);
//        // Inflate the layout for this fragment
//        this.context = container.getContext();
//        kb_helper = new KeyboardHelper();
//        kb_helper.setupUI(rootview.findViewById(R.id.Rl_mainf), this.activity);
//        kb_helper.setupUI(rootview.findViewById(R.id.f_logof), this.activity);
//        kb_helper.setupUI(rootview.findViewById(R.id.title_barf), this.activity);
//        kb_helper.setupUI(rootview.findViewById(R.id.sv_formf), this.activity);
//        kb_helper.setupUI(rootview.findViewById(R.id.lL_formf), this.activity);
//
//        tv_title.setText(R.string.Gunaso);
//        this.ddept_list = new ArrayList<String>();
//        ddept_list.add(" ");
//        this.dev_region_list = new ArrayList<String>();
//        dev_region_list.add(" ");
//
//        zoneId = "null";
//        regionId = "null";
//        deptId = "null";
//        districtId = "null";
//        sexId = "null";
//
//        this.sex = new ArrayList<String>();
//        this.library = new Library(this.context);
//
//        //IDMaps
//        areaId = new HashMap<>();
//        id_zone = new HashMap<>();
//        id_dept = new HashMap<>();
//        id_district = new HashMap<>();
//
//        this.sex.add("");
//        this.sex.add(this.activity.getApplicationContext().getString(R.string.Male));
//        this.sex.add(this.activity.getApplicationContext().getString(R.string.Female));
//
//        sp_sex.setAdapter(new FeedbackArrayAdapter(this.context, this.sex));
//
//        if (this.library.isConnectingToInternet()) {
//            getDept();
//
//            getArea();
//        } else {
//            Toast.makeText(this.context, "No Internet Connection", Toast.LENGTH_LONG).show();
//        }
//
//
////        this.sp_department.setAdapter(new FeedbackArrayAdapter(this,this.ddept_list));
//        this.sp_department.setOnItemSelectedListener(this);
//        this.sp_Area.setOnItemSelectedListener(this);
//        this.sp_zone.setOnItemSelectedListener(this);
//        this.sp_district.setOnItemSelectedListener(this);
//        this.sp_sex.setOnItemSelectedListener(this);
//        this.btn_proceed.setOnClickListener(this);
//        return rootview;
//    }
//
//    private void getArea() {
//
//        Call<AreaModel> getArea = apiService.getAreas("");
//        getArea.enqueue(new Callback<AreaModel>() {
//            @Override
//            public void onResponse(Call<AreaModel> call, Response<AreaModel> response) {
//
//                for (int i = 0; i < response.body().dataRegion.size(); i++) {
//                    int j = i;
//                    String id = response.body().dataRegion.get(i).id;
//                    String dev_reg = response.body().dataRegion.get(i).developmentRegion.toString().trim();
//
//                    String temp = covertDtoU(dev_reg);
//                    dev_region_list.add(temp);
//                    System.out.println("Converted devreg: " + id);
//                    areaId.put(++j, id);
//                }
//                if (dev_region_list != null)
//                    sp_Area.setAdapter(new FeedbackArrayAdapter(context, dev_region_list));
//            }
//
//            @Override
//            public void onFailure(Call<AreaModel> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
//
//    }
//
//    private void getDistrict(String regId, String zId) {
//        this.district_list = new ArrayList<String>();
//        district_list.add(" ");
//        sp_district.setAdapter(new FeedbackArrayAdapter(this.context, district_list));
//        Call<DistrictData> districts = apiService.getDistricts(regId, zId);
//        districts.enqueue(new Callback<DistrictData>() {
//            @Override
//            public void onResponse(Call<DistrictData> call, Response<DistrictData> response) {
//                if (response.body() != null) {
//                    for (int i = 0; i < response.body().dataDistrict.size(); i++) {
//                        int j = i;
//                        String Did = response.body().dataDistrict.get(i).id;
//                        String temp = response.body().dataDistrict.get(i).districtName.toString().trim();
//                        String district = covertDtoU(temp);
//                        System.out.println("Districts: " + district);
//                        district_list.add(district);
//                        id_district.put(++j, Did);
//
//                    }
//                }
//
//
//                sp_district.setAdapter(new FeedbackArrayAdapter(context, district_list));
//
//            }
//
//            @Override
//            public void onFailure(Call<DistrictData> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
//
//
//    }
//
//    private void getZone(String reg_id) {
//        zones_list = new ArrayList<>();
//        zones_list.add(" ");
//        sp_zone.setAdapter(new FeedbackArrayAdapter(this.context, zones_list));
//        Call<ZoneData> getZone = apiService.getZones(reg_id);
//        getZone.enqueue(new Callback<ZoneData>() {
//            @Override
//            public void onResponse(Call<ZoneData> call, Response<ZoneData> response) {
//                if (response.isSuccessful()) {
//                    List<DataZone> dataZone = response.body().dataZone;
//                    Log.d("size", dataZone.size() + "");
//                    for (int i = 0; i < dataZone.size(); i++) {
//                        int j = i;
//                        String idZone = response.body().dataZone.get(i).id;
//                        String zoneName = response.body().dataZone.get(i).zone.toString().trim();
//                        String zone = covertDtoU(zoneName);
//                        zones_list.add(zone);
//                        id_zone.put(++j, idZone);
//                        System.out.println("Zones: " + zone);
//                    }
//                    sp_zone.setAdapter(new FeedbackArrayAdapter(context, zones_list));
//                } else {
//                    Log.e("Test", "error: ");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ZoneData> call, Throwable t) {
//                Log.e("Test", "onFailure: ");
//            }
//        });
//
//    }
//
//
//    private void getDept() {
//        Call<DataClass> getdept = apiService.getMinitryInfo("");
//        getdept.enqueue(new Callback<DataClass>() {
//
//            @Override
//            public void onResponse(Call<DataClass> call, Response<DataClass> response) {
//                for (int i = 0; i < response.body().data.size(); i++) {
//                    int j = i;
//                    Ministry list = new Ministry();
//                    String MName = response.body().data.get(i).minitryName.toString().trim();
//                    String id = response.body().data.get(i).ministryID;
//                    String phone = response.body().data.get(i).ministryPhoneNumber;
//                    String addresstemp = response.body().data.get(i).ministryAddress.trim();
//
//                    String name = covertDtoU(MName);
//                    String address = covertDtoU(addresstemp);
//                    id_dept.put(++j, id);
//
//                    ddept_list.add(name);
//
//
//                }
//                sp_department.setAdapter(new FeedbackArrayAdapter(context, ddept_list));
//
//            }
//
//            @Override
//            public void onFailure(Call<DataClass> call, Throwable t) {
//
//            }
//        });
//
//    }
//
//    @Override
//    public int getLayout() {
//        return R.layout.activity_feedback;
//    }
//
//    public String covertDtoU(String s) {
//        StringBuilder sb = new StringBuilder();
//        List<String> list = new ArrayList<String>();
//        StringBuilder temp = new StringBuilder();
//        String[] strList = s.split(":");
//        for (int i = 0; i < strList.length; i++) {
//            temp.append(strList[i]);
//        }
//
//        String tempdept = temp.toString();
//        String[] deptList = tempdept.split(" ");
//        for (String str : deptList) {
//            str = str.trim();
//            if (!str.isEmpty()) {
//                String myString = str.replace("&#", "");
//                String[] split = myString.split(";");
//                for (int i = 0; i < split.length; i++) {
//                    sb.append((char) Integer.parseInt(split[i]));
//
//                }
//                sb.append(" ");
//            }
//        }
//        list.add(sb.toString());
//        System.out.println("Converted is " + sb.toString());
//        return sb.toString();
//
//
//    }
//
//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//        Spinner spinner = (Spinner) parent;
//        if (spinner.getId() == R.id.sp_departmentf) {
//            Log.d("The current position is: ", "position " + position);
//            deptId = id_dept.get(position);
//
//        } else if (spinner.getId() == R.id.sp_areaf) {
//
//            regionId = areaId.get(position);
//            Log.d("ZoneId: ", "id " + regionId + " " + position);
//            if (this.library.isConnectingToInternet())
//                getZone(regionId);
//
//
//        } else if (spinner.getId() == R.id.sp_zonef) {
//
//            zoneId = id_zone.get(position);
//            if (this.library.isConnectingToInternet())
//                getDistrict(regionId, zoneId);
//
//        } else if (spinner.getId() == R.id.sp_districtf) {
//            districtId = id_district.get(position);
//
//        } else if (spinner.getId() == R.id.sp_sexf) {
//            sexId = String.valueOf(position);
//        }
//
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.btn_proceedf) {
//            if (this.library.isConnectingToInternet()) {
//                if (deptId != null &&
//                        regionId != null &&
//                        zoneId != null &&
//                        districtId != null &&
//                        sexId != null &&
//                        !this.et_app_name.getText().toString().isEmpty() &&
//                        !this.et_subject.getText().toString().isEmpty() &&
//                        !this.et_address.getText().toString().isEmpty() &&
//                        !this.et_description.getText().toString().isEmpty()) {
//
//                    String department = deptId;
//                    String Subject = et_subject.getText().toString();
//                    String Name = et_app_name.getText().toString();
//                    String Address = et_address.getText().toString();
//                    String Description = et_description.getText().toString();
//                    String region = regionId;
//                    String zone = zoneId;
//                    String district = districtId;
//                    String sex = sexId;
//
//                    HashMap<String, String> formValues = new HashMap<String, String>();
//                    formValues.put("department", department);
//                    formValues.put("Subject", Subject);
//                    formValues.put("Name", Name);
//                    formValues.put("Address", Address);
//                    formValues.put("Description", Description);
//                    formValues.put("region", region);
//                    formValues.put("zone", zone);
//                    formValues.put("district", district);
//                    formValues.put("sex", sex);
//
//                    PostData(formValues);
//
//                } else {
//                    Toast.makeText(this.context, "Please complete the form", Toast.LENGTH_LONG).show();
//                }
//            } else {
//                Toast.makeText(this.context, "No Internet Connection", Toast.LENGTH_LONG).show();
//            }
//
//        }
//    }
//
//    public void PostData(HashMap<String, String> formValues) {
//
//        Call<PostModel> post = apiService.formPost(formValues);
//        post.enqueue(new Callback<PostModel>() {
//            @Override
//            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
//                sp_department.setSelection(0);
//                sp_Area.setSelection(0);
//                sp_zone.setSelection(0);
//                sp_district.setSelection(0);
//                sp_sex.setSelection(0);
//                et_app_name.getText().clear();
//                et_subject.getText().clear();
//                et_address.getText().clear();
//                et_description.getText().clear();
//
//                Toast.makeText(context, response.body().success.toString(), Toast.LENGTH_LONG).show();
//                Log.d("Success", response.body().success.toString());
//                check = true;
//
//            }
//
//            @Override
//            public void onFailure(Call<PostModel> call, Throwable t) {
//                Log.d("Failure", "Success");
//                check = false;
//            }
//        });
//
//    }
//
//
//}
