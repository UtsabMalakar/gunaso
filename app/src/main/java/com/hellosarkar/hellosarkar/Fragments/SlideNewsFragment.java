package com.hellosarkar.hellosarkar.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellosarkar.hellosarkar.NewsDataTest;
import com.hellosarkar.hellosarkar.R;

import java.util.ArrayList;

/**
 * Created by pradip on 12/22/16.
 */

public class SlideNewsFragment extends Fragment {

    public static SlideNewsFragment create(ArrayList<NewsDataTest> itemList, int position) {
        SlideNewsFragment fragment = new SlideNewsFragment();
        Bundle args = new Bundle();
        args.putInt("POSITION", position);
        args.putParcelableArrayList("LIST", itemList);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.full_news_content, container, false);

        ImageView image = (ImageView) rootView.findViewById(R.id.full_news_image);
        TextView news = (TextView) rootView.findViewById(R.id.full_news_txt_content);
        TextView date = (TextView) rootView.findViewById(R.id.full_news_date);
        TextView title = (TextView) rootView.findViewById(R.id.full_news_title);

        int position = getArguments().getInt("POSITION", 0);
        ArrayList<NewsDataTest> mItemList = getArguments().getParcelableArrayList("LIST");

        image.setImageResource(mItemList.get(position).getImage());
        news.setText(mItemList.get(position).getNews());
        date.setText(mItemList.get(position).getDateTime());
        title.setText(mItemList.get(position).getNewsTitle());

        return rootView;
    }
}
