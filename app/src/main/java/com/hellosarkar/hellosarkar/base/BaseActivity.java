package com.hellosarkar.hellosarkar.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hellosarkar.hellosarkar.retrofitConfig.ApiService;
import com.hellosarkar.hellosarkar.retrofitConfig.RetrofitSingleton;

import butterknife.ButterKnife;

/**
 * Created by ebpearls on 7/4/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected ApiService apiService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        apiService = RetrofitSingleton.getApiService();
        ButterKnife.bind(this);

    }

    public abstract int getLayout();

}
