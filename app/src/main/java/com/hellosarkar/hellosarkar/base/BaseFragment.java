//package com.hellosarkar.hellosarkar.base;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.hellosarkar.hellosarkar.retrofitConfig.ApiService;
//import com.hellosarkar.hellosarkar.retrofitConfig.RetrofitSingleton;
//
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//
///**
// * Created by ebpearls on 6/22/2016.
// */
//public abstract class BaseFragment extends Fragment {
//    protected Unbinder unbinder;
//    protected ApiService apiService;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(getLayout(), container, false);
//        unbinder = ButterKnife.bind(this, view);
//        apiService = RetrofitSingleton.getApiService();
//        return view;
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        unbinder.unbind();
//    }
//
//    protected abstract int getLayout();
//}
