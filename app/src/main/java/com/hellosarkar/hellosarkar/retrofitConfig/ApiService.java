package com.hellosarkar.hellosarkar.retrofitConfig;

import com.hellosarkar.hellosarkar.RetrofitModel.AreaModel;
import com.hellosarkar.hellosarkar.RetrofitModel.DataClass;
import com.hellosarkar.hellosarkar.RetrofitModel.DistrictData;
import com.hellosarkar.hellosarkar.RetrofitModel.PostModel;
import com.hellosarkar.hellosarkar.RetrofitModel.ZoneData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ebpearls on 7/4/2016.
 */
public interface ApiService {

    @FormUrlEncoded
    @POST("admin/mantralaya.php")
    Call<DataClass> getMinitryInfo(@Field("test") String test);

    @FormUrlEncoded
    @POST("admin/area.php")
    Call<AreaModel> getAreas(@Field("") String test1);

    @GET("admin/zone.php/")
    Call<ZoneData> getZones(@Query("region_id") String id);

    @GET("admin/district.php")
    Call<DistrictData> getDistricts(@Query("region_id") String id,
                                    @Query("zone_id") String zoneId);

    @FormUrlEncoded
    @POST("admin/post_gunaso.php")
    Call<PostModel> formPost(@FieldMap HashMap<String, String> params);


}
