package com.hellosarkar.hellosarkar.Adapters;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellosarkar.hellosarkar.NewsDataTest;
import com.hellosarkar.hellosarkar.R;
import com.hellosarkar.hellosarkar.utilities.Opener;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by pradip on 12/20/16.
 */

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.MyViewHOlder> {

    private List<NewsDataTest> itemList;
    private Activity context;
    CardView cardView;

    public NewsRecyclerAdapter(List<NewsDataTest> itemList) {
        this.itemList = itemList;
    }

    @Override
    public MyViewHOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        context= (Activity) parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_news_list, parent, false);
        MyViewHOlder myViewHOlder = new MyViewHOlder(view);
        return myViewHOlder;
    }

    @Override
    public void onBindViewHolder(final MyViewHOlder holder, int position) {
        holder.newsText.setText(itemList.get(position).getNews());
//        holder.newsImage.setImageResource(itemList.get(position).getImage());
        holder.newsDateTime.setText(itemList.get(position).getDateTime());

        Picasso.with(context)
                .load(itemList.get(position).getImage())
                .error(R.drawable.btn_icon)
                .into(holder.newsImage);
        holder.newsTitle.setText(itemList.get(position).getNewsTitle());

//        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/PT_Serif-Web-Regular.ttf");
//        holder.newsText.setTypeface(font);
//        holder.newsDateTime.setTypeface(font);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyViewHOlder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView newsText;
        ImageView newsImage;
        TextView newsDateTime;
        TextView newsTitle;

        public MyViewHOlder(View itemView) {
            super(itemView);

            newsText = (TextView) itemView.findViewById(R.id.news_text);
            newsImage = (ImageView) itemView.findViewById(R.id.news_image);
            newsDateTime = (TextView) itemView.findViewById(R.id.news_date_time);
            newsTitle = (TextView) itemView.findViewById(R.id.news_title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Opener.openFullNewsActivity(context, itemList, getAdapterPosition());
        }
    }

}
