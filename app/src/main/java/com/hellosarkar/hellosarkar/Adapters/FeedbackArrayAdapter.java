package com.hellosarkar.hellosarkar.Adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.hellosarkar.hellosarkar.R;

import java.util.List;

public class FeedbackArrayAdapter implements SpinnerAdapter {

    private Context context;
    private List<String> data;

    public FeedbackArrayAdapter(Context context, List<String> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(android.R.layout.simple_spinner_dropdown_item, null);
        }
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Ananda_Lipi_Bold_Cn_Bt.ttf");
        ((TextView) view).setTypeface(tf);
        ((TextView) view).setText(data.get(i));
        ((TextView) view).setTextColor(this.context.getResources().getColor(R.color.black));
        view.setPadding(10, 10, 10, 10);
        view.setBackgroundColor(this.context.getResources().getColor(R.color.white));
        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) View.inflate(context, android.R.layout.simple_spinner_item, null);
        textView.setText(data.get(i));
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Ananda_Lipi_Bold_Cn_Bt.ttf");
        textView.setTypeface(tf);
        textView.setTextColor(this.context.getResources().getColor(R.color.black));
        textView.setPadding(10, 10, 10, 10);
        textView.setBackgroundColor(this.context.getResources().getColor(R.color.white));
        return textView;
    }

    @Override
    public int getItemViewType(int i) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

}
