package com.hellosarkar.hellosarkar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.hellosarkar.hellosarkar.Adapters.FeedbackArrayAdapter;
import com.hellosarkar.hellosarkar.RetrofitModel.AreaModel;
import com.hellosarkar.hellosarkar.RetrofitModel.DataClass;
import com.hellosarkar.hellosarkar.RetrofitModel.DataZone;
import com.hellosarkar.hellosarkar.RetrofitModel.DistrictData;
import com.hellosarkar.hellosarkar.RetrofitModel.Ministry;
import com.hellosarkar.hellosarkar.RetrofitModel.PostModel;
import com.hellosarkar.hellosarkar.RetrofitModel.ZoneData;
import com.hellosarkar.hellosarkar.base.BaseActivity;
import com.hellosarkar.hellosarkar.utilities.KeyboardHelper;
import com.hellosarkar.hellosarkar.utilities.Library;
import com.hellosarkar.hellosarkar.utilities.Opener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String LOGTAG = "Gunaso";
    public String latitude, longitude;
    public String zoneId, regionId, deptId, districtId, sexId;
    public boolean flag_gps = false;
    //Binding Views
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.sp_department)
    Spinner sp_department;
    @BindView(R.id.sp_zone)
    Spinner sp_zone;
    @BindView(R.id.sp_area)
    Spinner sp_Area;
    @BindView(R.id.sp_district)
    Spinner sp_district;
    @BindView(R.id.sp_sex)
    Spinner sp_sex;
    @BindView(R.id.et_subject)
    EditText et_subject;
    @BindView(R.id.et_app_name)
    EditText et_app_name;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.et_discription)
    EditText et_description;
    @BindView(R.id.btn_proceed)
    Button btn_proceed;
    private Context context;
    //Lists
    private List<String> ddept_list;
    private List<String> dev_region_list;
    private List<String> zones_list;

//    LocationManager locationManager;
//    String provider;
    private List<String> district_list;
    private List<String> sex;
    private Map<Integer, String> areaId;
    private Map<Integer, String> id_zone;
    private Map<Integer, String> id_district;
    private Map<Integer, String> id_dept;
    private Library library;
    private KeyboardHelper kb_helper;
    private boolean check = false;
    private ProgressDialog dialog;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//         locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//         provider = locationManager.getBestProvider(criteria, false);


        tv_title.setText(R.string.Gunaso);
        this.ddept_list = new ArrayList<String>();
        ddept_list.add(" ");
        this.dev_region_list = new ArrayList<String>();
        kb_helper = new KeyboardHelper();
        kb_helper.setupUI(findViewById(R.id.Rl_main), this);
        kb_helper.setupUI(findViewById(R.id.f_logo), this);
        kb_helper.setupUI(findViewById(R.id.title_bar), this);
        kb_helper.setupUI(findViewById(R.id.sv_form), this);
        kb_helper.setupUI(findViewById(R.id.lL_form), this);
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        dev_region_list.add(" ");


        this.context = this;
//        dialog = new ProgressDialog(this);
//        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        dialog.setMessage("Loading...");
//        dialog.setIndeterminate(true);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();

        zoneId = "null";
        regionId = "null";
        deptId = "null";
        districtId = "null";
        sexId = "null";

        this.sex = new ArrayList<String>();
        this.library = new Library(this);

        //IDMaps
        areaId = new HashMap<>();
        id_zone = new HashMap<>();
        id_dept = new HashMap<>();
        id_district = new HashMap<>();

        this.sex.add("");
        this.sex.add(getApplicationContext().getString(R.string.Male));
        this.sex.add(getApplicationContext().getString(R.string.Female));

        sp_sex.setAdapter(new FeedbackArrayAdapter(this, this.sex));

        checkGPS();


        if (this.library.isConnectingToInternet()) {
            getDept();


            getArea();
//            dialog.cancel();
        } else {
            Toast.makeText(FeedbackActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
        }


//        this.sp_department.setAdapter(new FeedbackArrayAdapter(this,this.ddept_list));
        this.sp_department.setOnItemSelectedListener(this);
        this.sp_Area.setOnItemSelectedListener(this);
        this.sp_zone.setOnItemSelectedListener(this);
        this.sp_district.setOnItemSelectedListener(this);
        this.sp_sex.setOnItemSelectedListener(this);
        this.btn_proceed.setOnClickListener(this);


//        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/Ananda_Lipi_Bold_Cn_Bt.ttf");
//        tv_title.setTypeface(tf);

    }

    private void getArea() {

        Call<AreaModel> getArea = apiService.getAreas("");
        getArea.enqueue(new Callback<AreaModel>() {
            @Override
            public void onResponse(Call<AreaModel> call, Response<AreaModel> response) {

                for (int i = 0; i < response.body().dataRegion.size(); i++) {
                    int j = i;
                    String id = response.body().dataRegion.get(i).id;
                    String dev_reg = response.body().dataRegion.get(i).developmentRegion.toString().trim();

                    String temp = covertDtoU(dev_reg);
                    dev_region_list.add(temp);
                    System.out.println("Converted devreg: " + id);
                    areaId.put(++j, id);
                }
                if (dev_region_list != null)
                    sp_Area.setAdapter(new FeedbackArrayAdapter(FeedbackActivity.this, dev_region_list));
            }

            @Override
            public void onFailure(Call<AreaModel> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void getDistrict(String regId, String zId) {
        this.district_list = new ArrayList<String>();
        district_list.add(" ");
        sp_district.setAdapter(new FeedbackArrayAdapter(FeedbackActivity.this, district_list));
        Call<DistrictData> districts = apiService.getDistricts(regId, zId);
        districts.enqueue(new Callback<DistrictData>() {
            @Override
            public void onResponse(Call<DistrictData> call, Response<DistrictData> response) {
                if (response.body() != null) {
                    for (int i = 0; i < response.body().dataDistrict.size(); i++) {
                        int j = i;
                        String Did = response.body().dataDistrict.get(i).id;
                        String temp = response.body().dataDistrict.get(i).districtName.toString().trim();
                        String district = covertDtoU(temp);
                        System.out.println("Districts: " + district);
                        district_list.add(district);
                        id_district.put(++j, Did);

                    }
                }
                sp_district.setAdapter(new FeedbackArrayAdapter(FeedbackActivity.this, district_list));

            }

            @Override
            public void onFailure(Call<DistrictData> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    private void getZone(String reg_id) {
        zones_list = new ArrayList<>();
        zones_list.add(" ");
        sp_zone.setAdapter(new FeedbackArrayAdapter(FeedbackActivity.this, zones_list));
        Call<ZoneData> getZone = apiService.getZones(reg_id);
        getZone.enqueue(new Callback<ZoneData>() {
            @Override
            public void onResponse(Call<ZoneData> call, Response<ZoneData> response) {
                if (response.isSuccessful()) {
                    List<DataZone> dataZone = response.body().dataZone;
                    Log.d("size", dataZone.size() + "");
                    for (int i = 0; i < dataZone.size(); i++) {
                        int j = i;
                        String idZone = response.body().dataZone.get(i).id;
                        String zoneName = response.body().dataZone.get(i).zone.toString().trim();
                        String zone = covertDtoU(zoneName);
                        zones_list.add(zone);
                        id_zone.put(++j, idZone);
                        System.out.println("Zones: " + zone);
                    }
                    sp_zone.setAdapter(new FeedbackArrayAdapter(FeedbackActivity.this, zones_list));
                } else {
                    Log.e("Test", "error: ");
                }
            }

            @Override
            public void onFailure(Call<ZoneData> call, Throwable t) {
                Log.e("Test", "onFailure: ");
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
//        locationManager.requestLocationUpdates(provider, 400, 1, (android.location.LocationListener) this);

    }


    private void
    getDept() {
        Call<DataClass> getdept = apiService.getMinitryInfo("");
        getdept.enqueue(new Callback<DataClass>() {

            @Override
            public void onResponse(Call<DataClass> call, Response<DataClass> response) {
                for (int i = 0; i < response.body().data.size(); i++) {
                    int j = i;
                    Ministry list = new Ministry();
                    String MName = response.body().data.get(i).minitryName.toString().trim();
                    String id = response.body().data.get(i).ministryID;
                    String phone = response.body().data.get(i).ministryPhoneNumber;
                    String addresstemp = response.body().data.get(i).ministryAddress.trim();
                    if (!response.body().data.get(i).ministryID.equals("461")&& !response.body().data.get(i).ministryID.equals("462")) {
                        String name = covertDtoU(MName);

                        String address = covertDtoU(addresstemp);
                        id_dept.put(++j, id);

                        ddept_list.add(name);
                    }
                    if (response.body().data.get(i).ministryID.equals("462")) {

                        ddept_list.add(MName);
                    }

                }
                sp_department.setAdapter(new FeedbackArrayAdapter(FeedbackActivity.this, ddept_list));

            }

            @Override
            public void onFailure(Call<DataClass> call, Throwable t) {

            }
        });

    }

    @Override
    public int getLayout() {
        return R.layout.activity_feedback;
    }

    public String covertDtoU(String s) {
        StringBuilder sb = new StringBuilder();
        List<String> list = new ArrayList<String>();
        StringBuilder temp = new StringBuilder();
        String[] strList = s.split(":");
        for (int i = 0; i < strList.length; i++) {
            temp.append(strList[i]);
        }

        String tempdept = temp.toString();
        String[] deptList = tempdept.split(" ");
        for (String str : deptList) {
            str = str.trim();
            if (!str.isEmpty()) {
                String myString = str.replace("&#", "");
                String[] split = myString.split(";");
                for (int i = 0; i < split.length; i++) {
                    sb.append((char) Integer.parseInt(split[i]));

                }
                sb.append(" ");
            }
        }
        list.add(sb.toString());
        System.out.println("Converted is " + sb.toString());
        return sb.toString();


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.sp_department) {
            Log.d("The current position is: ", "position " + position);
            deptId = id_dept.get(position);

        } else if (spinner.getId() == R.id.sp_area) {
            if(sp_Area.getSelectedItemPosition()!=0) {
                regionId = areaId.get(position);
                Log.d("ZoneId: ", "id " + regionId + " " + position);
                if (this.library.isConnectingToInternet())
                    getZone(regionId);
            }


        } else if (spinner.getId() == R.id.sp_zone) {
            if (sp_zone.getSelectedItemPosition()!=0) {
                zoneId = id_zone.get(position);
                if (this.library.isConnectingToInternet())
                    getDistrict(regionId, zoneId);
            }

        } else if (spinner.getId() == R.id.sp_district) {
            districtId = id_district.get(position);

        } else if (spinner.getId() == R.id.sp_sex) {
            sexId = String.valueOf(position);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_proceed) {


            if (this.library.isConnectingToInternet()) {
                if (deptId != null && !deptId.equals("0") &&
                        regionId != null && !regionId.equals("0") &&
                        zoneId != null && !zoneId.equals("0") &&
                        districtId != null && !districtId.equals("0") &&
                        sexId != null && !sexId.equals("0") &&
                        !this.et_app_name.getText().toString().isEmpty() &&
                        !this.et_subject.getText().toString().isEmpty() &&
                        !this.et_address.getText().toString().isEmpty() &&
                        !this.et_description.getText().toString().isEmpty()) {


                    String department = deptId;
                    String Subject = et_subject.getText().toString();
                    String Name = et_app_name.getText().toString();
                    String Address = et_address.getText().toString();
                    String Description = et_description.getText().toString();
                    String region = regionId;
                    String zone = zoneId;
                    String district = districtId;
                    String sex = sexId;

                    HashMap<String, String> formValues = new HashMap<String, String>();
                    formValues.put("department", department);
                    formValues.put("Subject", Subject);
                    formValues.put("Name", Name);
                    formValues.put("Address", Address);
                    formValues.put("Description", Description);
                    formValues.put("region", region);
                    formValues.put("zone", zone);
                    formValues.put("district", district);
                    formValues.put("sex", sex);
//                    if (checkGPS()){
//
//                    }
//                    else{
//                        buildAlertMessageNoGps();
//                    }


                    formValues.put("lat", latitude);
                    formValues.put("lng", longitude);
//                    Toast.makeText(FeedbackActivity.this, "Latitude: " + latitude + " Longitude: " + longitude, Toast.LENGTH_SHORT).show();
                    Log.d(LOGTAG, "After Submit: Latitude: " + latitude);
                    Log.d(LOGTAG, "After Submit: Longitude: " + longitude);


                    PostData(formValues);


                } else {
                    Toast.makeText(FeedbackActivity.this, "Please complete the form", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(FeedbackActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
            }

        }
    }

    public void PostData(HashMap<String, String> formValues) {
        btn_proceed.setEnabled(false);

        Call<PostModel> post = apiService.formPost(formValues);
        post.enqueue(new Callback<PostModel>() {
            @Override
            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                sp_department.setSelection(0);
                sp_Area.setSelection(0);
                sp_zone.setSelection(0);
                sp_district.setSelection(0);
                sp_sex.setSelection(0);
                et_app_name.getText().clear();
                et_subject.getText().clear();
                et_address.getText().clear();
                et_description.getText().clear();


//                Toast.makeText(FeedbackActivity.this,response.body().success.toString() , Toast.LENGTH_LONG).show();
                new AlertDialog.Builder(FeedbackActivity.this)
                        .setTitle("Success")
                        .setMessage(response.body().success.toString())
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(FeedbackActivity.this, "Thank you", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setIcon(R.drawable.ic_logo_icon)
                        .show();
                Log.d("Success", response.body().success.toString());
                btn_proceed.setEnabled(true);
                check = true;

            }

            @Override
            public void onFailure(Call<PostModel> call, Throwable t) {
                Log.d("Failure", "Success");
                check = false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        Opener.openMainActivity(FeedbackActivity.this);
    }

//    private void  setMyLocation() {
//        if(this.library.isConnectingToInternet()) {
//            Log.d(LOGTAG, "location button clicked!");
//            final ProgressDialog pd = ProgressDialog.show(this, "", "Loading location...", true, true);
//
//            MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult() {
//                @Override
//                public void gotLocation(Location location) {
//                    //Got the location!
//                    if (location != null) {
//                        pd.dismiss();
//                        //get latitude and longitude of the location
//                        double lng = location.getLongitude();
//                        double lat = location.getLatitude();
//
//                        latitude = Double.toString(lat);
//                        longitude = Double.toString(lng);
//
//                        Log.d(LOGTAG, "gotLocation:latitude: "+latitude);
//
//                        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
//
//                        try {
//                            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
//
//                            if (!addresses.isEmpty()) {
//                                Address returnedAddress = addresses.get(0);
//                                StringBuilder strReturnedAddress = new StringBuilder();
//                                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
//                                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(" ");
//                                }
//                                String location_text = strReturnedAddress.toString();
//                                Log.d("Location success:", location_text);
//
//
//                            } else {
//                                Toast.makeText(context, "No Location found", Toast.LENGTH_LONG).show();
//                            }
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                            Toast.makeText(context, "Timed out waiting for response from server. Please try again.", Toast.LENGTH_LONG).show();
////                                location_txt.setText("Cannot get Address!");
//                        }
//
//                    } else {
//
//                        Toast.makeText(context, "No Location found", Toast.LENGTH_LONG).show();
//                        pd.dismiss();
//                    }
//
//                }
//            };
//            MyLocation myLocation = new MyLocation();
//            myLocation.getLocation(this.context, locationResult);
//            if (!myLocation.gps_enabled && !myLocation.network_enabled) {
////                pd.dismiss();
//                // Build the alert dialog
//                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//                builder.setTitle("Location Services Not Active");
//                builder.setMessage("Please enable Location Services and GPS");
//                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        // Show location settings when the user acknowledges the alert dialog
//                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(intent);
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                Dialog alertDialog = builder.create();
//                alertDialog.setCanceledOnTouchOutside(true);
//                alertDialog.show();
////				showMessage();
//            }
//        }
//        else{
//            Toast.makeText(context,"No internet connection",Toast.LENGTH_LONG).show();
//        }
//    }

//        public void setMyLocation(){
//
//
//            Location location = locationManager.getLastKnownLocation(provider);
//
//            // Initialize the location fields
//            if (location != null) {
//                System.out.println("Provider " + provider + " has been selected.");
//                onLocationChanged(location);
//            } else {
//                latitude = "";
//                longitude = "";
//            }
//
//
//    }
    private void permissionCheckLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && this.checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && this.checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1200);
            return;
        } else {
            checkGPS();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 1200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkGPS();
                } else {
                    latitude = "";
                    longitude = "";
                }
                break;

        }
    }

    public void checkGPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
            }
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
//            setMyLocation();
        }

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 100);
                        permissionCheckLocation();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        latitude = "";
                        longitude = "";
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            permissionCheckLocation();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());
        latitude = String.valueOf(lat);
        longitude = String.valueOf(lng);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        latitude = "";
        longitude = "";
    }
}
