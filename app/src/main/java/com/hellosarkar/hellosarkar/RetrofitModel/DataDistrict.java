package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 7/6/2016.
 */
public class DataDistrict {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("zone_id")
    @Expose
    public String zoneId;
    @SerializedName("dev_region_id")
    @Expose
    public String devRegionId;
    @SerializedName("district_name")
    @Expose
    public String districtName;
}
