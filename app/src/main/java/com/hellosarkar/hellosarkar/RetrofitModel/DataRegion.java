package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 7/6/2016.
 */
public class DataRegion {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("development_region")
    @Expose
    public String developmentRegion;
}
