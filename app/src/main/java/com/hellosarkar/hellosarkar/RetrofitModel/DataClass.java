package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 7/5/2016.
 */
public class DataClass {

    @SerializedName("data")
    @Expose
    public List<Ministry> data = new ArrayList<Ministry>();

}
