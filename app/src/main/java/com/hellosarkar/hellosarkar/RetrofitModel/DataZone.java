package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 7/6/2016.
 */
public class DataZone {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("dev_region_id")
    @Expose
    public String devRegionId;
    @SerializedName("zone")
    @Expose
    public String zone;
}
