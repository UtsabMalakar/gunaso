package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pradip on 12/20/16.
 */

public class NewsModel {

    @SerializedName("news_id")
    @Expose
    public String id;

    @SerializedName("published_date")
    @Expose
    public String publishedDate;

    @SerializedName("feat_img")
    @Expose
    public String imageUrl;

    @SerializedName("content")
    @Expose
    public String textContent;
}
