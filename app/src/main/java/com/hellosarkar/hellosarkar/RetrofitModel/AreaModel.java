package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 7/6/2016.
 */
public class AreaModel {

    @SerializedName("data_region")
    @Expose
    public List<DataRegion> dataRegion = new ArrayList<DataRegion>();
}
