package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 7/5/2016.
 */
public class Ministry {
    @SerializedName("ministryID")
    @Expose
    public String ministryID;
    @SerializedName("minitryName")
    @Expose
    public String minitryName;
    @SerializedName("ministryPhoneNumber")
    @Expose
    public String ministryPhoneNumber;
    @SerializedName("ministryAddress")
    @Expose
    public String ministryAddress;
}
