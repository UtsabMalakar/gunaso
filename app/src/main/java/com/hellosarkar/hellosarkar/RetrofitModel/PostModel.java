package com.hellosarkar.hellosarkar.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 7/7/2016.
 */
public class PostModel {

    @SerializedName("success")
    @Expose
    public String success;
}
