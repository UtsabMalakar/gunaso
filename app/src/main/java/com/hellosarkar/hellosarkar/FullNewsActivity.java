package com.hellosarkar.hellosarkar;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

import com.hellosarkar.hellosarkar.Fragments.SlideNewsFragment;
import com.hellosarkar.hellosarkar.utilities.Opener;

import java.util.ArrayList;

/**
 * Created by pradip on 12/21/16.
 */

public class FullNewsActivity extends FragmentActivity {

    Intent intent;
    ArrayList<NewsDataTest> itemList = new ArrayList<NewsDataTest>();
    int adapterPosition;
    ViewPager viewPager;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_news);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        intent = getIntent();
        itemList = intent.getParcelableArrayListExtra("ITEM_LIST");
        adapterPosition = intent.getIntExtra("ADAPTER_POSITION", 0);

        viewPager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(adapterPosition, true);

    }


    @Override
    public void onBackPressed() {
        Opener.openNewsListActivity(FullNewsActivity.this);

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return SlideNewsFragment.create(itemList, position);

        }

        @Override
        public int getCount() {
            return itemList.size();

        }
    }

}
